function constructOptions(options, selector, selected_option) {
    for (option in options) {
        var optionElement = document.createElement("option");
        optionElement.text = option;
        optionElement.value = options[option];
        if(selected_option == options[option]){
            optionElement.selected = true;
        }
        selector.add(optionElement);
    }
    
  }

function playWarning(){
    chrome.storage.sync.get('sound', function(data) {
        playSound( data.sound );
    });
}

function playSound(sound){
    let audioFile = chrome.runtime.getURL(sound);
    var audio = new Audio(audioFile);
    audio.volume = 0.5;
    audio.play();
}