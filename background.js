'use strict';

chrome.runtime.onInstalled.addListener(function() {
    chrome.storage.sync.set(
        {repairEndWarning: true}, 
        function() {
            console.log("Warning enabled.");
        }
    );

    chrome.storage.sync.set(
        {sound: 'sounds/bowl_tibetan_short.mp3'}, 
        function() {
            console.log("Default sound: Bowl tibetan short.");
        }
    );
    
    chrome.declarativeContent.onPageChanged.removeRules(
        undefined, 
        function() {
            chrome.declarativeContent.onPageChanged.addRules(
                [{
                    conditions: [
                        new chrome.declarativeContent.PageStateMatcher({
                            pageUrl: {queryContains: 'action=repair'},
                        }),
                        new chrome.declarativeContent.PageStateMatcher({
                            pageUrl: {queryContains: 'action=Upgrade'},
                            }),
                        new chrome.declarativeContent.PageStateMatcher({
                            pageUrl: {pathSuffix: 'console'},
                            })
                    ],
                    actions: [new chrome.declarativeContent.ShowPageAction()]
                }]
            );
        });    
});

chrome.webNavigation.onCompleted.addListener(function(details) {
    chrome.tabs.executeScript(
        details.tabId, {
            file: 'warning.js'
        });
},
        {
            url: [{queryContains: 'action=repair'}, {pathSuffix: 'console'}]});