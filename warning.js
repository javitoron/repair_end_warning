chrome.storage.sync.get('repairEndWarning', function(data) {
      if(data.repairEndWarning){
          playWarning();
          alert(1);
          window.scrollBy(0, 1000);
      }
      else{
          //do nothing;
      }
  });

//TODO: Usar estas 2 funciones que ya están en lib.js
function playWarning(){
    chrome.storage.sync.get('sound', function(data) {
        playSound( data.sound );
    });
}

function playSound(sound){
    let audioFile = chrome.runtime.getURL(sound);
    var audio = new Audio(audioFile);
    audio.volume = 0.5;
    audio.play();
}