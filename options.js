setup_playButton();
chrome.storage.sync.get('sound', function(data) {
    setup_soundsSelector( data.sound );
});

function setup_soundsSelector(str_mp3){
    let soundsSelector = document.getElementById('soundsSelector');

    constructOptions(sounds, soundsSelector, str_mp3);    

    soundsSelector.addEventListener('change', function() {
        chrome.storage.sync.set({sound: soundsSelector.value}, function() {
          console.log('New selected sound is ' + soundsSelector.value);
        })
      });
}


function setup_playButton(){
    let soundsSelector = document.getElementById('soundsSelector');
    let playButton = document.getElementById('playButton');

    playButton.addEventListener('click', function() {
        playSound(soundsSelector.options[soundsSelector.selectedIndex].value);
      });
}