 let toggleWarning = document.getElementById('toggleWarning');

  chrome.storage.sync.get('repairEndWarning', function(data) {
      if(data.repairEndWarning){
          toggleWarning.classList.remove('disabled');
          toggleWarning.classList.add('enabled');
      }
      else{
          toggleWarning.classList.add('disabled');
          toggleWarning.classList.remove('enabled');          
      }
  });

  toggleWarning.onclick = function(element) {
      let warning = true;
      if(toggleWarning.classList.contains('disabled')){
          warning = false;
      }

      if(warning){
          toggleWarning.classList.add('disabled');
          toggleWarning.classList.remove('enabled');          
          chrome.storage.sync.set(
                {repairEndWarning: false}, 
                function() {
                    console.log("Warning disabled.");
                }
            );
      }
      else{
          toggleWarning.classList.remove('disabled');
          toggleWarning.classList.add('enabled');
           chrome.storage.sync.set(
                {repairEndWarning: true}, 
                function() {
                    console.log("Warning enabled.");
                }
            );
     }
      
/*      chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
          chrome.tabs.executeScript(
            tabs[0].id,
              {code: "var audio = new Audio('" + audioFile + "'); audio.volume = 0.05; audio.play();"});
    });*/
  };

//      let audioFile = chrome.runtime.getURL('sounds/bell_high.mp3');
//          {code: "var audio = new Audio('" + audioFile + "'); audio.volume = 0.05; audio.play();"});
